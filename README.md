# **Tous les Documents - Algorithmique Parallèle et Distribuée**

Tous les Documents (Cours, TDs, TPs, MiniProjets, ...) dans le cadre du cours d'Algorithmique Parallèle et Distribuée à **POLYTECH GRENOBLE**.

**[BONFILS Antoine](https://gitlab.com/AntoineBF)** (INFO4)

***

## **TABLE DE NAVIGATION**

* **[Cours](#table-des-cours)**

* **[TDs](#table-des-tds)**

***

## **TABLE DES [COURS](./Cours/)**

| Cours | Traités  |
|:-----:|:--------:|
| [Chapitre 0 - Les 8 algorithmes](./Cours/chap0_algos.pdf) | :heavy_check_mark: |
| [Chapitre 1 - Le commencement](./Cours/chap1_APD.pdf) | :heavy_check_mark: |
| [Chapitre 2 - Election sur Anneau](./Cours/chap2_APD.pdf) | :heavy_check_mark: |
| [Chapitre 3 - Causalité Horloges de Lamport](./Cours/chap3_APD.pdf) | :heavy_check_mark: |
| [Chapitre 4 - Algorithme à vagues](./Cours/chap4_APD.pdf) | :heavy_check_mark: |
| [Chapitre 5 - Etat global](./Cours/chap5_APD.pdf) | :heavy_check_mark: |
***

## **TABLE DES [TDs](./TD/)**

| TDs  | Traités  | Compte-Rendu  | Corrections / Solutions dispo |
|:----------:|:--------:|:---------:|:-----------------------------:|
| [TD1 - Processus](./TD/TD1_processus/td1_sujet.pdf) | :heavy_check_mark: | [Reponse](./TD/TD1_processus/td1_compte_rendu.md) | :x: |
| [TD2 & 3 - Sinalgo](./TD/TD2_sinalgo/td2-3.pdf) | :heavy_check_mark: | [Code](./TD/TD2_sinalgo/sinalgo/) et [Reponse](./TD/TD2_sinalgo/td2-3_compte_rendu.md) | :x: |
| [TD4 - Election dans un anneau: Algo de Peterson](./TD/TD4/td4.pdf) | :heavy_check_mark: | [Reponse](./TD/TD4/td4_compte_rendu.md) | :x: |
| [TD5/DM - Que le plus grand gagne!](./TD/TD_5_DM/apd-dm-2023.pdf) | :heavy_check_mark: | [Code](./TD/TD2_sinalgo/sinalgo/src/projects/devoir/) et [Reponse](./TD/TD_5_DM/td5_DM_compte_rendu.md) | :x: |
| [TD6 & 7 - Variante de l'algo d'exclusion mutuelle de Lamport](./TD/TD6_7/td6-7.pdf) | :heavy_check_mark: | [Code](./TD/TD2_sinalgo/sinalgo/src/projects/mutexRicartAgrawala/) et [Reponse](./TD/TD6_7/td6-7_compte_rendu.md) | :x: |
| [TD8 & 9 - Circulation de jeton dans un réseau quelconque](./TD/TD8_9/td8-9.pdf) | :heavy_check_mark: | [Code]() et [Reponse](./TD/TD8_9/td8-9_compte_rendu.md) | :x: |

***

### Legende:

+ :heavy_check_mark: : trait
+ :heavy_minus_sign: : en cours
+ :x: : non disponible
+ Reponse: que des réponses à des questions
+ Code: du code à été créé

***