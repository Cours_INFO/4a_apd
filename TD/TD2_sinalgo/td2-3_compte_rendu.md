# Algorithmique Parallèle Distribuée

## **TD2 & 3 - Sinalgo**

### **I. Installation**

Installation de l'environnement

### **II. Organisation**

Explication sur le fonctionnement de Sinalgo

### **III. Le projet Walker**

Explication du projet Walker sur son fonctionnement et son interface

### **IV. Programmer d'autres projets**

### **IV.1. Inondation**

1. Voir le fichier [description.txt](./sinalgo/src/projects/inondation/description.txt)

2. Voir le package [Inondation](./sinalgo/src/projects/inondation/)

### **IV.2. Election sur anneau**

1. Voir [Algorithme 2](../../Cours/chap0_algos.pdf)

2. Voir le package [Election](./sinalgo/src/projects/election/)

3. Complexité de l'algorithme d'élection de Le Lann pour un processus:

| Complexité en mémoire | Complexité en temps |
|:---------------------:|:-------------------:|
| O(n) | O(nlog(n)) |

Pour plus de détail : Voir le cours [élection sur l'anneau](../../Cours/chap2_APD.pdf)

4. L'hypothèse sur l'ordre FIFO des canaux est **necessaire**.