package projects.mutexRicartAgrawala.nodes.messages;

import projects.mutexRicartAgrawala.nodes.nodeImplementations.MutexRANode;

public class MutexRAMessage extends sinalgo.nodes.messages.Message {

	/** date of the message */
	private int date;
	
	/** node which sent the message */
	private MutexRANode from;
	
	/** Type: ASK (for critical section), AUTHOR(ization) to access critical section */
	public static enum Label { ASK, AUTHOR };
	private Label label;
	
	public MutexRAMessage(Label label, int date, MutexRANode from) {
		super();
		this.date = date;
		this.from = from;
		this.label = label;
	}

	/**
	 * This function returns the date of the event
	 * 
	 * @return The date of the month.
	 */
	public int getDate() {
		return date;
	}
	
	/**
	 * This function returns the node from which the edge originates
	 * 
	 * @return The from node.
	 */
	public MutexRANode getFrom() {
		return from;
	}

	/**
	 * This function returns the ID of the user who sent the message
	 * 
	 * @return The ID of the node that the edge is coming from.
	 */
	public int getFromID() {
		return from.ID;
	}

	/**
	 * This function returns the label of the node
	 * 
	 * @return The label is being returned.
	 */
	public Label getLabel() { 
		return label; 
	}

	/**
	 * This function returns a clone of the message. Since the message is immutable, we can just return
	 * the message itself.
	 * 
	 * @return The message itself.
	 */
	public sinalgo.nodes.messages.Message clone() {
		return this;
	}

	public String toString() {
		return label.toString() + "(" + date + ")-from-" + from.ID;
	}
}