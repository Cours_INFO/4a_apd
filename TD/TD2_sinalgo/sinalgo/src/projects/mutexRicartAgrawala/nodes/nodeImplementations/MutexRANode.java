package projects.mutexRicartAgrawala.nodes.nodeImplementations;

import java.awt.Color;
import java.util.*;

import projects.mutexRicartAgrawala.CustomGlobal;
import projects.mutexRicartAgrawala.nodes.messages.*;
import projects.mutexRicartAgrawala.nodes.timers.*;
import sinalgo.nodes.Node;
import sinalgo.nodes.edges.Edge;

public class MutexRANode extends sinalgo.nodes.Node {
	
	/** Variables from the algorithm */
	// TO DO
	private boolean demandeur;
	private int dd = 0;
	private int h;
	//private ArrayList<Boolean> DR;
	private MutexRANode[] dr = new MutexRANode[CustomGlobal.SIZE];
	private int nb_perm;
	/** ---------------------------- */
	
	/** total order on requests: returns true when 'this' request has priority over parameter. */
	private boolean has_priority (int processID, int processRequestDate) {
		if(this.dd < processRequestDate) {
			return true;
		} else if(this.dd == processRequestDate) {
			if(this.ID < processID) {
				return true;
			}
		}
		return false; // TO DO
		/** ---------------------------- */		
	}
	
	public static int timerRefresh = 100;
	private static Random random = new Random();

	public void init() {
		setColor(Color.YELLOW);
		(new InitTimer(this)).startRelative(random.nextInt(10) + 1, this);
	}

	private boolean initiated = false;
	
	public void initiate() {
		if (!initiated) {
			initiated = true;
			/* Init of variables */
			this.demandeur = false;
			this.h = 0;
			
			for(int i=0;i<CustomGlobal.SIZE;i++) {
				dr[i] = null;
			}
			
			Iterator<Edge> it = outgoingConnections.iterator();
			while (it.hasNext()) {
				// loop over every other nodes
				it.next();
				/*
				sinalgo.nodes.messages.Message msg = inbox.next();
				if (msg instanceof MutexRAMessage) {
					MutexRAMessage mutex = (MutexRAMessage) msg;
					this.h = Math.max(this.h, mutex.getDate());
					this.treatMessage(mutex);
					this.h++;
				}*/
			}
			/** ---------------------------- */
			
			/* testing scenario: every process is asking from the beginning */
			ask();

			/* other scenarios ... */
			/*
			(new AskTimer(this)).startRelative(random.nextInt(3 * MutexRANode.timerRefresh) 
			 + MutexRANode.timerRefresh, this);
			 */
			/*
			if (ID == 3) (new AskTimer(this)).startRelative(2 * MutexRANode.timerRefresh, this); 		
			if (ID == 3) (new AskTimer(this)).startRelative(4 * MutexRANode.timerRefresh, this); 		
			if (ID == 1) (new AskTimer(this)).startRelative(4 * MutexRANode.timerRefresh, this); 		
			 */
		}
	}
	
	public String toString() {
		return " " + ID; // TO DO: add useful values 
	}
	
	public void ask() {
		/* Algorithm for Request */
		// TO DO
		if(this.demandeur == false) {
			System.out.println("****" + this.ID + " is asking!!!!!!!!!!!!!!!!!");
			setColor(Color.BLUE);

			broadcast(new MutexRAMessage(MutexRAMessage.Label.ASK, this.h, this));
			
			this.dd = this.h;
			this.demandeur = true;
			this.nb_perm = 0;
			this.h++;
			/*
			int degree = outgoingConnections.size();
			if (degree == 0) throw new RuntimeException("no neighbor");
			sinalgo.tools.storage.ReusableListIterator<sinalgo.nodes.edges.Edge> iter 
				= outgoingConnections.iterator();
			while(iter.hasNext())
				send(new MutexRAMessage(MutexRAMessage.Label.ASK, this.h, this), iter.next().endNode); //TODO
				*/
		}
		/** ---------------------------- */
	}

	public void handleMessages(sinalgo.nodes.messages.Inbox inbox) {
		if (!initiated) initiate();

		while(inbox.hasNext()) {

			sinalgo.nodes.messages.Message msg = inbox.next();
			System.out.println(this.ID + " treats msg " + msg);
			if (msg instanceof MutexRAMessage) {
				treatMessage((MutexRAMessage) msg);
			}
			
		}
	}
	
	private void treatMessage(MutexRAMessage msg) {
	
		/* Algorithm when receiving a message */
		// TO DO
		// each time you send another message, use:
		//   				System.out.println("-> " + this.ID + " sends msg "); // ... to q
		// each time process gets CS, use: 
		// 				System.out.println("---> " + ID + " got critical section!");
		
		// ...
		this.h = Math.max(this.h, msg.getDate());
		
		switch (msg.getLabel()) {
			case ASK: 
				if(this.demandeur == true && this.has_priority(msg.getFromID(), msg.getDate())) {
					this.dr[msg.getFromID()-1] = msg.getFrom();
					//this.DR.add(msg.getFromID(), true);
				} else {
					send(new MutexRAMessage(MutexRAMessage.Label.AUTHOR, this.h, this), msg.getFrom());
				}
				break;
			case AUTHOR: 
				this.nb_perm++;
				if(this.nb_perm == CustomGlobal.SIZE-1) {	//TODO sus
					//TODO SC
					System.out.println("---> " + ID + " got critical section!");
					this.demandeur = false;
					for(int i=1;i <= CustomGlobal.SIZE;i++) {
						if(dr[i-1] != null) {
							send(new MutexRAMessage(MutexRAMessage.Label.AUTHOR, this.h, this), dr[i-1]);
							System.out.println("-> " + this.ID + " sends msg ");
							dr[i-1] = null;
						}
					}
					/*
					while(msg.getFromID() != this.ID) {
						if(this.DR.get(msg.getFromID()).booleanValue()) {
							send(new MutexRAMessage(MutexRAMessage.Label.AUTHOR, this.h, this), this.research(msg.getFromID()));
							this.DR.set(msg.getFromID(), false);
						}
					}*/
				}
				break;
		}
		this.h++;
		// ...
		/** ---------------------------- */
	}
	
	public void preStep() {};
	public void neighborhoodChange() {};
	public void postStep() {}; 
	public void checkRequirements() throws sinalgo.configuration.WrongConfigurationException {};
	public void draw(java.awt.Graphics g, sinalgo.gui.transformation.PositionTransformation pt, 
					 boolean highlight) {
		// draw the node as a circle with the text inside
		super.drawNodeAsDiskWithText(g, pt, highlight, toString(), 20, Color.black);
	}
}