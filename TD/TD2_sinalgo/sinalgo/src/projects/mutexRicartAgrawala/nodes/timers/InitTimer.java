package projects.mutexRicartAgrawala.nodes.timers;

import projects.mutexRicartAgrawala.nodes.nodeImplementations.MutexRANode;

/** When does the node wake up and start executing it */
public class InitTimer extends sinalgo.nodes.timers.Timer {
	
	private MutexRANode node;
	
	public InitTimer(MutexRANode node) {
			this.node = node;
	}
		
	public void fire() {
		node.initiate();
	}
}