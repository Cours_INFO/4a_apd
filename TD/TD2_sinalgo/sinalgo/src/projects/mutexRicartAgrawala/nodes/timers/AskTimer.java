package projects.mutexRicartAgrawala.nodes.timers;

import projects.mutexRicartAgrawala.nodes.nodeImplementations.MutexRANode;

/** Send request, repeatedly */
public class AskTimer extends sinalgo.nodes.timers.Timer {
		
	private MutexRANode node;
	
	public AskTimer(MutexRANode node) {
		this.node = node;
	}
		
	/* the function "fire" is called when the timer is over */
	public void fire() {
		node.ask();
	}
}