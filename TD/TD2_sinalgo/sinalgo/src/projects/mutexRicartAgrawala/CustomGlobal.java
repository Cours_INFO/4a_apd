package projects.mutexRicartAgrawala;

import projects.mutexRicartAgrawala.nodes.nodeImplementations.*;
import sinalgo.runtime.AbstractCustomGlobal;
import sinalgo.nodes.*;

public class CustomGlobal extends AbstractCustomGlobal{
	public static final int SIZE = 5;
	
	public boolean hasTerminated() {
		return false;
	}

	/** Button to create a clique network. */
	@AbstractCustomGlobal.CustomButton(buttonText="Build a Clique Network")
	public void cliqueButton() {
		buildClique(SIZE);
	}

	private void addEdge(Node from, Node to) {
		from.addConnectionTo(to);
		to.addConnectionTo(from);
	}

	private void buildClique(int numOfNodes) {
		sinalgo.tools.Tools.removeAllNodes();		

		// the center
		double centerPosX = sinalgo.configuration.Configuration.dimX / 2;
		double centerPosY = sinalgo.configuration.Configuration.dimY / 2;
	
		// nodes
		MutexRANode [] theNodes = new MutexRANode[numOfNodes];	
		double initAngle = 2 * Math.PI / numOfNodes;
		double range = 350.0;
		double angle = 0;
		for(int i = 0; i < numOfNodes; i++){
			double posX = centerPosX + range * Math.cos(angle);
			double posY = centerPosY + range * Math.sin(angle);
			MutexRANode node = new MutexRANode();
			node.setPosition(posX, posY, 0);
			node.finishInitializationWithDefaultModels(true);
			theNodes[i] = node;
			angle += initAngle;
		}
		
		// edges
		for (int i = 0; i < numOfNodes; i++)
			for (int j = 0; j < numOfNodes; j++)
				if (i != j) addEdge(theNodes[i], theNodes[j]);
		
		// Repaint the GUI as we have added some nodes
		sinalgo.tools.Tools.repaintGUI();
	}
}