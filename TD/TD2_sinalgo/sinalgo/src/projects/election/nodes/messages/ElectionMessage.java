package projects.election.nodes.messages;

import sinalgo.nodes.messages.Message;

public class ElectionMessage extends Message {

	private static int msgCounter = 0;
	private int msgId;
	public ElectionMessage(int ID) {
		super();
		msgId = ID; //msgCounter;
		msgCounter++;
	}

	public Message clone() {
		return this;
	}

	public int getID() {
		return this.msgId;
	}
	
	public String toString() {
		return "election" + msgId;
	}
}