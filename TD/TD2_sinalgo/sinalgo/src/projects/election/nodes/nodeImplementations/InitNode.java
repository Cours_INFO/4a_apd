package projects.election.nodes.nodeImplementations;

import java.awt.Color;

import projects.election.nodes.messages.ElectionMessage;
import projects.election.nodes.timers.InitTimer;
import sinalgo.nodes.Node;

/** the initiator node sends the message (the walker) */
public class InitNode extends ElectionNode {
	Node neighbor;
	int minID = this.ID;
	
	/* InitNode() { ... } */
	public void init() {
		super.init();
		setColor(Color.YELLOW);
		(new InitTimer(this)).startRelative(InitTimer.timerRefresh, this);
	}

	public void initiate() {
		ElectionMessage election = new ElectionMessage(this.ID);
		System.out.println(this + " is sending now message " + election);
		neighbor = neighborfinder(outgoingConnections);
		
		send(election, neighbor);
	}

	public void handleMessages(sinalgo.nodes.messages.Inbox inbox) {
		while(inbox.hasNext()) {
			sinalgo.nodes.messages.Message msg = inbox.next();
			if (msg instanceof ElectionMessage) {
				ElectionMessage election = (ElectionMessage) msg;
				int IDmsg = election.getID();
				if (IDmsg == this.ID) {			// the message went around the ring
					if (minID == this.ID) {		// and if this ID is the smaller
						setColor(Color.GREEN);	// then, it is The Leader
					} else {
						setColor(Color.ORANGE);	//else
					}
					/*
				} else {
					send(election, neighbor);*/
				}
				if(IDmsg < minID) {
					minID = IDmsg;
					send(election, neighbor);
				}
				System.out.println(this + " received message " + election + 
										   " and sends it now to " + neighbor);
			}
		}
	}

	public String toString() {
		return super.toString();	// + "(init)"
	}
}
