package projects.election.nodes.nodeImplementations;

import java.awt.Color;
import java.util.Random;

import projects.election.nodes.messages.ElectionMessage;
import sinalgo.nodes.Node;

public class ElectionNode extends sinalgo.nodes.Node {

	/* WalkerNode() { 
	 *   // no constructor code, it breaks the way sinalgo builds the nodes. 
	 *   // instead use the init() method 
	 * }
	 * */
	public int state;
	public void init() {
		setColor(Color.YELLOW);
		this.state = 0;
	}
		
	public String toString() {
		return " " + ID + " "; 
	}

	public void handleMessages(sinalgo.nodes.messages.Inbox inbox) {
		while(inbox.hasNext()) {
			sinalgo.nodes.messages.Message msg = inbox.next();
			if (msg instanceof ElectionMessage) {
				Node next = neighborfinder(outgoingConnections);
				//for(int i = 0; i < outgoingConnections.size(); i++) {
					ElectionMessage election = (ElectionMessage) msg;
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}/*
					if(((ElectionNode) next).state == 0) {*/
						send(election, next);
						((ElectionNode) next).state = 1;
					//}
					System.out.println(this + " received message " + election + 
										   " and sends it now to " + next);
				//}
			}
		}
	}
	
	Node neighborfinder(sinalgo.nodes.Connections neighbors) {
		int degree = neighbors.size();
		if (degree == 0) throw new RuntimeException("no neighbor");
		sinalgo.tools.storage.ReusableListIterator<sinalgo.nodes.edges.Edge> iter 
			= neighbors.iterator();
		Node node = iter.next().endNode;/*
		for (int i = 1; i < degree; i++)
			if(((ElectionNode) node).state == 1) {
				node = iter.next().endNode;
			}*/
		//setColor(Color.MAGENTA);
		return node;
	}

	
	public void preStep() {};
	public void neighborhoodChange() {};
	public void postStep() {}; 
	public void checkRequirements() throws sinalgo.configuration.WrongConfigurationException {};
	public void draw(java.awt.Graphics g, sinalgo.gui.transformation.PositionTransformation pt, 
					 boolean highlight) {
		// draw the node as a circle with the text inside
		super.drawNodeAsDiskWithText(g, pt, highlight, toString(), 20, Color.black);
	}
}