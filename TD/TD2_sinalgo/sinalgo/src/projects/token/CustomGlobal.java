package projects.token;

import projects.token.nodes.nodeImplementations.*;
import sinalgo.nodes.Node;
import sinalgo.runtime.AbstractCustomGlobal;

public class CustomGlobal extends AbstractCustomGlobal{
	
	public boolean hasTerminated() {
		return false;
	}

	/** Button to create a grid network. */
	@AbstractCustomGlobal.CustomButton(buttonText="Build Grid Network")
	public void gridButton() {
		int length = 6;
		int width = 10;
		buildGrid(length, width);
	}

	/** Button to create a star network. */
	@AbstractCustomGlobal.CustomButton(buttonText="Build Star Network")
	public void starButton() {
		int stages = 4;
		int degree = 5;
		buildStar(degree, stages);
	}

	/** Button to create a star network. */
	@AbstractCustomGlobal.CustomButton(buttonText="Build Giga Star Network")
	public void gigaStarButton() {
		int stages = 5;
		int degree = 10;
		buildStar(degree, stages);
	}

	/** Button to create a tree network. */
	@AbstractCustomGlobal.CustomButton(buttonText="Build a Tree Network")
	public void treeButton() {
		buildTree(3, 4);
	}

	/** Button to create a tree network. */
	@AbstractCustomGlobal.CustomButton(buttonText="Build a Giga Tree Network")
	public void gigaTreeButton() {
		buildTree(5, 4);
	}

	/** Button to create a ring network. */
	@AbstractCustomGlobal.CustomButton(buttonText="Build a Ring Network")
	public void ringButton() {
		buildRing(15);
	}

	private void addEdge(Node from, Node to) {
		from.addConnectionTo(to);
		to.addConnectionTo(from);
	}

	private void buildGrid(int length, int width) {		
		sinalgo.tools.Tools.removeAllNodes();
		if (length <= 0 || width <= 0) 
			throw new RuntimeException("negative length or width for grid");

		//size of the screen to print the nodes : 
		// sinalgo.configuration.Configuration.dimX (same with dimY)		
		
		// nodes
		sinalgo.tools.Tools.removeAllNodes();
		TokenNode [][] theNodes = new TokenNode[length][width];
				
		double deltaX = sinalgo.configuration.Configuration.dimX / (length + 1);
		double deltaY = sinalgo.configuration.Configuration.dimY / (width + 1);
		double posX = deltaX;
		for (int x = 0; x < length; x++) {
			double posY = deltaY;
			for (int y = 0; y < width; y++) {
				TokenNode node;
				if (x == 0 && y == 0) { 
					node = new InitTokenNode();
				}
				else node = new TokenNode();
				node.setPosition(posX, posY, 0);
				node.finishInitializationWithDefaultModels(true);				
				theNodes[x][y] = node;
				posY += deltaY;
			}
			posX += deltaX;
		}
		
		// connections
		for (int x = 0; x < length; x++) { 
			for (int y = 1; y < width; y++) 
				addEdge(theNodes[x][y - 1], theNodes[x][y]); 
		}
		for (int y = 0; y < width; y++) { 
			for (int x = 1; x < length; x++) 
				addEdge(theNodes[x - 1][y], theNodes[x][y]); 
		}

		// Repaint the GUI as we have added some nodes
		sinalgo.tools.Tools.repaintGUI();
	}

	private void buildStar(int degree, int numStages) {
		sinalgo.tools.Tools.removeAllNodes();		

		// nodes
		TokenNode [][] theNodes = new TokenNode[degree][numStages];
		TokenNode center;
		
		// the center
		double centerPosX = sinalgo.configuration.Configuration.dimX / 2;
		double centerPosY = sinalgo.configuration.Configuration.dimY / 2;
		center = new TokenNode();
		center.setPosition(centerPosX, centerPosY, 0);
		center.finishInitializationWithDefaultModels(true);
		
		// the star...
		double initAngle = 2 * Math.PI / degree;
		double initRange = 100.0;		
		double range = 0.0;
		TokenNode node = center;
		for(int i = 1; i <= numStages; i++){
			range += initRange;
			double angle = 0;
			for(int j = 0; j < degree; j++){
				double posX = centerPosX + range * Math.cos(angle);
				double posY = centerPosY + range * Math.sin(angle);
				if (i ==  numStages && j == 0) node = new InitTokenNode();
				else node = new TokenNode();
				node.setPosition(posX, posY, 0);
				node.finishInitializationWithDefaultModels(true);
				theNodes[j][i-1] = node;
				angle += initAngle;
			}
		}
		
		// connections
		for(int j=0; j<degree; j++) { // per branch
			addEdge(center, theNodes[j][0]);
			for(int i=1; i<numStages; i++) {
				addEdge(theNodes[j][i-1], theNodes[j][i]);
			}
		}
		for(int j=0; j<degree; j++) { 
			// stage 1
			addEdge(theNodes[j][0], theNodes[(j+1)%degree][0]);
			addEdge(theNodes[j][0], theNodes[(j+2)%degree][0]);
			addEdge(theNodes[j][1], theNodes[(j+1)%degree][0]);
			addEdge(theNodes[(j+1)%degree][1], theNodes[(j)%degree][0]);
		}
		
		// Repaint the GUI as we have added some nodes
		sinalgo.tools.Tools.repaintGUI();
	}

	private void buildTree(int degree, int height) {
		sinalgo.tools.Tools.removeAllNodes();		

		// nodes
		TokenNode [][] theNodes = new TokenNode[height][];
		TokenNode center;
		
		// the center
		double centerPosX = sinalgo.configuration.Configuration.dimX / 2;
		double centerPosY = sinalgo.configuration.Configuration.dimY / 2;
		center = new InitTokenNode();
		center.setPosition(centerPosX, centerPosY, 0);
		center.finishInitializationWithDefaultModels(true);
		
		theNodes[0] = new TokenNode[1];
		theNodes[0][0] = center;
		
		// the star...
		double initAngle = 2 * Math.PI;
		double initRange = 100.0;		
		double range = 0.0;
		int localDegree = 1;
		TokenNode node = center;
		for(int i = 1; i < height; i++){
			localDegree = localDegree * degree;
			theNodes[i] = new TokenNode[localDegree];
			initAngle = initAngle / degree;
			range += initRange;
			double angle = 0.0;
			for(int j = 0; j < localDegree; j++){
				double posX = centerPosX + range * Math.cos(angle);
				double posY = centerPosY + range * Math.sin(angle);
				node = new TokenNode();
				node.setPosition(posX, posY, 0);
				node.finishInitializationWithDefaultModels(true);
				theNodes[i][j] = node;
				angle += initAngle;
			}
		}
		
		// connections
		for(int h = 0; h < height - 1; h++) {
			int idx = 0;
			for (int p = 0; p < theNodes[h].length; p++) {
				for (int j = idx; j < idx + degree; j++) {
					addEdge(theNodes[h][p], theNodes[h+1][j]);				
				}
				idx += degree;
			}
		}
		
		// Repaint the GUI as we have added some nodes
		sinalgo.tools.Tools.repaintGUI();
	}

	private void buildRing(int numOfNodes) {
		sinalgo.tools.Tools.removeAllNodes();		

		// nodes
		TokenNode [] theNodes = new TokenNode[numOfNodes];
	
		// the center
		double centerPosX = sinalgo.configuration.Configuration.dimX / 2;
		double centerPosY = sinalgo.configuration.Configuration.dimY / 2;
	
		// the ring...
		double initAngle = 2 * Math.PI / numOfNodes;
		double range = 200.0;
		double angle = 0;
		for(int i = 0; i < numOfNodes; i++){
			double posX = centerPosX + range * Math.cos(angle);
			double posY = centerPosY + range * Math.sin(angle);
			TokenNode node;
			if (i ==  0) node = new InitTokenNode();
			else node = new TokenNode();
			node.setPosition(posX, posY, 0);
			node.finishInitializationWithDefaultModels(true);
			if (i > 0) addEdge(theNodes[i - 1], node);
			theNodes[i] = node;
			angle += initAngle;
		}
		addEdge(theNodes[0], theNodes[numOfNodes - 1]);
		// Repaint the GUI as we have added some nodes
		sinalgo.tools.Tools.repaintGUI();
	}}