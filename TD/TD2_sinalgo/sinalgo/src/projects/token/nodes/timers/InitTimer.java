package projects.token.nodes.timers;

import projects.token.nodes.nodeImplementations.*;

/** When does the node wake up and start executing it */
public class InitTimer extends sinalgo.nodes.timers.Timer {
	
	private TokenNode node;
	
	public InitTimer(TokenNode node) {
			this.node = node;
	}
		
	public void fire() {
		node.initiate();
	}
}