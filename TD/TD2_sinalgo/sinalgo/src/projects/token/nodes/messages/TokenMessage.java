package projects.token.nodes.messages;

public class TokenMessage extends sinalgo.nodes.messages.Message {

	public TokenMessage() {
		super();
	}

	public sinalgo.nodes.messages.Message clone() {
		return this;
	}
	
	public String toString() {
		return "token";
	}
}