package projects.token.nodes.nodeImplementations;

import java.awt.Color;
import java.util.*;

import projects.token.nodes.messages.*;
import projects.token.nodes.timers.*;
import sinalgo.nodes.Node;
import sinalgo.nodes.edges.Edge;

public class TokenNode extends sinalgo.nodes.Node {
	
	public static int timerRefresh = 100;
	private static Random random = new Random();
	
	public void init() {
		setColor(Color.YELLOW);
		(new InitTimer(this)).startRelative(random.nextInt(10) + 1, this);
	}

	/** is the node initiator? */
	boolean initiator = false;

	/** visited channels */
	HashMap <Integer, Boolean> visited;
	
	/** Parent; -1 if not initialized (BOT), -2 if root (TOP) */
	int parent;
	
	private boolean initiated = false;
	public void initiate() {
		if (!initiated) { 
			initiated = true;
			visited = new HashMap<Integer, Boolean>();
			Iterator<Edge> it = outgoingConnections.iterator();
			
			while (it.hasNext()) { visited.put(it.next().endNode.ID, false); }
			if(initiator) {
				it = outgoingConnections.iterator();
				Node node = it.next().endNode;
				visited.replace(node.ID, true);
				parent = -2;
				send(new TokenMessage(), node);
			} else {
				parent = -1;
			}
		} 
	}
	
	public String toString() {
		return "" + ID;
	}
	
	/**
	 * It treats the messages it receives
	 * 
	 * @param inbox the inbox of the node, containing all the messages that have been sent to this node.
	 */
	public void handleMessages(sinalgo.nodes.messages.Inbox inbox) {
		if (!initiated) initiate();

		while(inbox.hasNext()) {
			sinalgo.nodes.messages.Message msg = inbox.next();
			System.out.println(this.ID + " treats msg " + msg);
			if (msg instanceof TokenMessage) {
				treatMessage((TokenMessage) msg, inbox.getSender());
			}
		}
	}

	/** 
	 * It runs the token algorithm.
	 * 
	 * @param msg the message that is being sent
	 * @param q the node that sent the message
	 */
	private void treatMessage(TokenMessage msg, Node q) {
		/**
		 * If the node has not yet received a token, 
		 * it sets its parent to the node that sent it the token. 
		 */
		if(parent == -1) {
			parent = q.ID;
		}
		
		setColor(Color.ORANGE);
		
		/** 
		 * It checks if all of its neighbors have been visited.
		 */
		int count = 0;
		Iterator<Edge> it = outgoingConnections.iterator(); 
		boolean found = false;
		while(it.hasNext() && visited.get(it.next().endNode.ID)) {
			count++;
		}
		/**
		 * If they have, it takes this decision (sets its color to blue).
		 */
		if(count == outgoingConnections.size()) {
			setColor(Color.BLUE);	// decide
		
		/**
		 * If the node that sent the message isn't the parent, and 
		 * the node that sent the message has been not yet visited, 
		 * it changes this boolean in the HashMap and sends the token to 
		 * this node.
		 */
		} else if (parent != q.ID && !(visited.get(q.ID))) {
			visited.replace(q.ID, true);
			send(new TokenMessage(), q);
		
		/**
		 * It checks on all of its neighbors if it exist at least one neighbor 
		 * which isn't parent of the node that sent the message, and has been not yet visited.
		 * If it is, it changes this boolean in the HashMap and sends the token to 
		 * this node.
		 */
		} else {
			it = outgoingConnections.iterator(); 
			found = false;
			Node q_prim;
			while((it.hasNext()) && found == false) {
				q_prim = it.next().endNode;
				if (q != q_prim && (parent != q_prim.ID && !(visited.get(q_prim.ID)))) {
					visited.replace(q_prim.ID, true);
					send(new TokenMessage(), q_prim);
					found = true;
				}
			}
			/**
			 * If nothing was found, it changes the boolean of parent in the HashMap 
			 * and sends the token to parent.
			 */
			if (found == false) {
				visited.replace(parent, true);
				it = outgoingConnections.iterator(); 
				Node q_tmp = null;
				while((it.hasNext()) && found == false) {
					q_tmp = it.next().endNode;
					if(q_tmp.ID == parent) {
						found = true;
					}
				}
				if(found) {
					send(new TokenMessage(), q_tmp);
				}
			}
		}
	}
	
	public void preStep() {};
	public void neighborhoodChange() {};
	public void postStep() {}; 
	public void checkRequirements() throws sinalgo.configuration.WrongConfigurationException {};
	public void draw(java.awt.Graphics g, sinalgo.gui.transformation.PositionTransformation pt, 
					 boolean highlight) {
		// draw the node as a circle with the text inside
		super.drawNodeAsDiskWithText(g, pt, highlight, toString(), 20, Color.black);
	}
}