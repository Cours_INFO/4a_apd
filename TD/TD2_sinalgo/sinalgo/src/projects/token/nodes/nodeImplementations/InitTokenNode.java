package projects.token.nodes.nodeImplementations;

import java.awt.Color;

import projects.token.nodes.timers.InitTimer;

/** the initiator node sends the message */
public class InitTokenNode extends TokenNode {

	/* InitNode() { ... } */
	public void init() {
		super.init(); 
		setColor(Color.GREEN);
		(new InitTimer(this)).startRelative(timerRefresh, this); 		
	}

	public void initiate() {
		initiator = true;
		super.initiate();
	}

	public String toString() {
		return super.toString() + "(init)";
	}
}
