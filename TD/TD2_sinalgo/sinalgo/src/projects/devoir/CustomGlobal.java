package projects.devoir;

import java.util.ArrayList;

import projects.devoir.nodes.nodeImplementations.InitNode;
import projects.devoir.nodes.nodeImplementations.DevoirNode;
import projects.devoir.nodes.nodeImplementations.*;
import sinalgo.nodes.Node;
import sinalgo.runtime.AbstractCustomGlobal;

/**
 * This class holds customized global state and methods for the framework. 
 * The only mandatory method to overwrite is 
 * <code>hasTerminated</code>
 * <br>
 * Optional methods to override are
 * <ul>
 * <li><code>customPaint</code></li>
 * <li><code>handleEmptyEventQueue</code></li>
 * <li><code>onExit</code></li>
 * <li><code>preRun</code></li>
 * <li><code>preRound</code></li>
 * <li><code>postRound</code></li>
 * <li><code>checkProjectRequirements</code></li>
 * </ul>
 * @see sinalgo.runtime.AbstractCustomGlobal for more details.
 * <br>
 * In addition, this class also provides the possibility to extend the framework with
 * custom methods that can be called either through the menu or via a button that is
 * added to the GUI. 
 */
public class CustomGlobal extends AbstractCustomGlobal{
	
	/* (non-Javadoc)
	 * @see runtime.AbstractCustomGlobal#hasTerminated()
	 */
	public boolean hasTerminated() {
		return false;
	}

	/** Button to create a ring network. */
	@AbstractCustomGlobal.CustomButton(buttonText="Build a Ring Network")
	public void ringButton() {
		buildRing(5);
	}

	/** Button to create a test ring network. */
	@AbstractCustomGlobal.CustomButton(buttonText="Build a Ring Network for test")
	public void testButton() {
		test_random_ID(10);
	}

	private void addEdge(Node from, Node to) {
		from.addConnectionTo(to);
		to.addConnectionTo(from);
	}

	/**
	 * It creates a ring of nodes, with the specified/ordered number of nodes
	 * 
	 * @param numOfNodes the number of nodes in the ring
	 */
	private void buildRing(int numOfNodes) {
		sinalgo.tools.Tools.removeAllNodes();

		// nodes
		//DevoirNode [] theNodes = new DevoirNode[numOfNodes];
		InitNode [] theNodes = new InitNode[numOfNodes];
	
		// the center
		double centerPosX = sinalgo.configuration.Configuration.dimX / 2;
		double centerPosY = sinalgo.configuration.Configuration.dimY / 2;
	
		// the ring...
		double initAngle = 2 * Math.PI / numOfNodes;
		double range = 200.0;
		double angle = 0;
		for(int i = 0; i < numOfNodes; i++){
			double posX = centerPosX + range * Math.cos(angle);
			double posY = centerPosY + range * Math.sin(angle);
			InitNode node = new InitNode();
			//DevoirNode node = new DevoirNode();
			node.setPosition(posX, posY, 0);
			node.finishInitializationWithDefaultModels(true);
			if (i > 0) addEdge(theNodes[i - 1], node);
			theNodes[i] = node;
			angle += initAngle;
		}
		addEdge(theNodes[numOfNodes - 1], theNodes[0]);
		// Repaint the GUI as we have added some nodes
		sinalgo.tools.Tools.repaintGUI();
	}
	
	/**
	 * It generates a random integer between 1 and 1000.
	 * 
	 * @return A random integer between 1 and 1000.
	 */
	private int randInt() {
        int Min = 1;
        int Max = 1000;
        int res = Min + (int)(Math.random() * ((Max - Min) + 1));
        return res;
    }
	
	/**
	 * It creates a ring of nodes, and assigns a random ID to each node
	 * 
	 * Use for safely tests
	 *
	 * @param numOfNodes the number of nodes in the network
	 */
	private void test_random_ID(int numOfNodes) {
        sinalgo.tools.Tools.removeAllNodes();

        // nodes
        //DevoirNode [] theNodes = new DevoirNode[numOfNodes];
        InitNode [] theNodes = new InitNode[numOfNodes];
    
        // the center
        double centerPosX = sinalgo.configuration.Configuration.dimX / 2;
        double centerPosY = sinalgo.configuration.Configuration.dimY / 2;
    
        // the ring...
        double initAngle = 2 * Math.PI / numOfNodes;
        double range = 200.0;
        double angle = 0;
        
        ArrayList<Integer> ListID = new ArrayList<Integer>(numOfNodes);
        
        for(int i = 0; i < numOfNodes; i++){
            double posX = centerPosX + range * Math.cos(angle);
            double posY = centerPosY + range * Math.sin(angle);
            InitNode node = new InitNode();
            node.setPosition(posX, posY, 0);
            node.finishInitializationWithDefaultModels(true);
            if (i > 0) addEdge(theNodes[i - 1], node);
            theNodes[i] = node;
            angle += initAngle;
            int newID = randInt();
            while(ListID.contains(newID)) { // check that all ID are unique
                newID = randInt();
            }
            node.ID = newID;
            ListID.add(newID);
        }
        addEdge(theNodes[numOfNodes - 1], theNodes[0]);
        // Repaint the GUI as we have added some nodes
        sinalgo.tools.Tools.repaintGUI();
    }
}