package projects.devoir.nodes.nodeImplementations;

import java.awt.Color;
import java.util.Random;

import projects.devoir.nodes.messages.DevoirMessage;
import sinalgo.nodes.Node;

public class DevoirNode extends sinalgo.nodes.Node {

	protected int l;
	private int n;
	private boolean resultat;
	
	public void init() {
		setColor(Color.YELLOW);
		this.l = 1;
		this.n = 0;
		this.resultat = false;
	}
		
	/**
	 * The toString() function returns the ID of node
	 * 
	 * @return The ID of the object.
	 */
	public String toString() {
		return " " + ID + " "; 
	}

	/**
	 * The above code is implementing the algorithm described in the paper.
	 *
	 * @param indox
	 */
	public void handleMessages(sinalgo.nodes.messages.Inbox inbox) {
		while(inbox.hasNext()) {
			setColor(Color.ORANGE);
			sinalgo.nodes.messages.Message msg = inbox.next();
			if (msg instanceof DevoirMessage) {
				DevoirMessage devoir = (DevoirMessage) msg;
				if (devoir.type == 0) {		// ASK
					if (devoir.id == this.ID) {
						this.resultat = true;
						setColor(Color.GREEN);
					} else if(devoir.l>0) {
						if(devoir.id>this.ID) {
							DevoirMessage newDevoir = new DevoirMessage(0, devoir.id, devoir.l-1);		// ASK
							if (inbox.getSender() == this.leftNeighbor(outgoingConnections)) {
								send(newDevoir,this.rightNeighbor(outgoingConnections));
							} else {
								send(newDevoir,this.leftNeighbor(outgoingConnections));
							}
						}
					} else if(devoir.l==0) {
						if(devoir.id>this.ID) {
							DevoirMessage newDevoir = new DevoirMessage(1, devoir.id);	// REPLY
							if (inbox.getSender() == this.leftNeighbor(outgoingConnections)) {
								send(newDevoir,this.leftNeighbor(outgoingConnections));
							} else {
								send(newDevoir,this.rightNeighbor(outgoingConnections));
							}
						}
					}
				} else if(devoir.type == 1) {	// REPLY
					if(devoir.id != this.ID) {
						DevoirMessage newDevoir = new DevoirMessage(1, devoir.id);	// REPLY
						if (inbox.getSender() == this.leftNeighbor(outgoingConnections)) {
							send(newDevoir,this.rightNeighbor(outgoingConnections));
						} else {
							send(newDevoir,this.leftNeighbor(outgoingConnections));
						}
					} else {
						this.n++;
						if(this.n == 2) {
							setColor(Color.BLACK);
							this.n = 0;
							this.l *= 2;
							DevoirMessage newDevoir = new DevoirMessage(0, this.ID, this.l-1);		// ASK
							send(newDevoir,this.rightNeighbor(outgoingConnections));
							send(newDevoir,this.leftNeighbor(outgoingConnections));
						} else {	setColor(Color.BLUE);	}
					}
				}
				/*
				System.out.println("*** " + devoir.id + " -> msg -> " + this.ID + " ***");
				System.out.println(this.ID + ": l: " + devoir.l);
				System.out.println(this.ID + ": type: " + devoir.type);
				*/
			}
			/*
			System.out.println(this.ID + ": resultat: " + this.resultat);
			System.out.println(this.ID + ": l: " + this.l);
			System.out.println(this.ID + ": n: " + this.n);
			System.out.println("***********************");
			*/
		}
	}

	/**
	 * "Return the right neighbor of the node."
	 * 
	 * The function takes a parameter of type `sinalgo.nodes.Connections`, which is a list of all
	 * neighbors of the node. The function first checks whether the node has any neighbors at all. If not,
	 * it throws an exception. Otherwise, it returns the right neighbor.
	 * 
	 * @param neighbors the list of neighbors of the current node.
	 * @return The node that is the right neighbor of the current node.
	 */
	Node rightNeighbor(sinalgo.nodes.Connections neighbors) {
		int degree = neighbors.size();
		if (degree == 0) throw new RuntimeException("no neighbor");
		sinalgo.tools.storage.ReusableListIterator<sinalgo.nodes.edges.Edge> iter 
			= neighbors.iterator();
		return iter.next().endNode;
	}
	
	/**
	 * "Return the left neighbor of the current node."
	 * 
	 * The function takes a parameter of type `sinalgo.nodes.Connections`, which is a list of all
	 * neighbors of the current node. The function first checks that the node has at least one neighbor.
	 * If not, it throws an exception. Otherwise, it returns the second neighbor in the list.
	 * 
	 * @param neighbors the list of neighbors of the current node
	 * @return The second neighbor of the node.
	 */
	Node leftNeighbor(sinalgo.nodes.Connections neighbors) {
		int degree = neighbors.size();
		if (degree == 0) throw new RuntimeException("no neighbor");
		sinalgo.tools.storage.ReusableListIterator<sinalgo.nodes.edges.Edge> iter 
			= neighbors.iterator();
		iter.next();
		return iter.next().endNode;
	}
	
	
	public void preStep() {};
	public void neighborhoodChange() {};
	public void postStep() {}; 
	public void checkRequirements() throws sinalgo.configuration.WrongConfigurationException {};
	public void draw(java.awt.Graphics g, sinalgo.gui.transformation.PositionTransformation pt, 
					 boolean highlight) {
		// draw the node as a circle with the text inside
		super.drawNodeAsDiskWithText(g, pt, highlight, toString(), 20, Color.black);
	}
}