package projects.devoir.nodes.nodeImplementations;

import java.awt.Color;

import projects.devoir.nodes.messages.DevoirMessage;
import projects.devoir.nodes.timers.InitTimer;
import sinalgo.nodes.Node;

/** the initiator node sends the message (the walker) */
public class InitNode extends DevoirNode {
	
	/* InitNode() { ... } */
	public void init() {
		super.init(); 
		//setColor(Color.GREEN);
		(new InitTimer(this)).startRelative(InitTimer.timerRefresh, this); 		
	}

	/**
	 * The initiator sends a message to its left and right neighbors
	 */
	public void initiate() {
		DevoirMessage devoir = new DevoirMessage(0, this.ID, this.l-1);
		System.out.println(this + " is sending now message " + devoir);

		send(devoir, rightNeighbor(outgoingConnections));
		send(devoir, leftNeighbor(outgoingConnections));	
	}

	public String toString() {
		return super.toString(); 	//+ "(init)";
	}
}
