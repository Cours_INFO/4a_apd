package projects.devoir.nodes.messages;

import sinalgo.nodes.messages.Message;

public class DevoirMessage extends Message {

    public int type; // 0 -> ASK ; 1 -> REPLY
    public int id;
    public int l; //length

    /**
     * Constructor for ASK message
     *
     * @param type message's type
     * @param id the id of the original node
     * @param l message's distance
     */
    public DevoirMessage(int type, int id, int l) {
        super();
        this.type = type;
        this.id = id;
        this.l = l;
    }

    /**
     * Constructor for REPLY message
     *
     * @param type message's type
     * @param id the id of the original node
     */
    public DevoirMessage(int type, int id) {
        super();
        this.type = type;
        this.id = id;
    }

    /**
     * This function returns a reference to the same object that it was called on.
     * 
     * @return The object itself.
     */
    public Message clone() {
        return this;
    }
    
    /**
     * The function returns a string that is the name of the class
     * 
     * @return The string "devoir"
     */
    public String toString() {
        return "devoir";
    }
}