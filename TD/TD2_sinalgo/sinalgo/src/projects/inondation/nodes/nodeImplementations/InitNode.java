package projects.inondation.nodes.nodeImplementations;

import java.awt.Color;

import projects.inondation.nodes.messages.InondationMessage;
import projects.inondation.nodes.timers.InitTimer;
import sinalgo.nodes.Node;

/** the initiator node sends the message (the walker) */
public class InitNode extends InondationNode {

	/* InitNode() { ... } */
	public void init() {
		super.init(); 
		setColor(Color.GREEN);
		(new InitTimer(this)).startRelative(InitTimer.timerRefresh, this); 		
	}

	public void initiate() {
		InondationMessage inondation = new InondationMessage();
		System.out.println(this + " is sending now message " + inondation);

		int degree = outgoingConnections.size();
		Node[] array_node = randomWalkChoice(outgoingConnections);
		for (int i = 0; i < degree; i++) {
			if(((InondationNode) array_node[i]).state == 0) {
				send(inondation, array_node[i]);
			}
		}
	}

	public String toString() {
		return super.toString() + "(init)";
	}
}
