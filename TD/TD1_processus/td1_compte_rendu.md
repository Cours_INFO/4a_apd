# Algorithmique Parallèle Distribuée

## **TD1 - Processus**

### **Question 1**

Après avoir exécuté le système pas à pas de façon synchrone, c’est-à-dire à chaque pas d’ex ́ecution, on peut constater qu'il existe un risque d'interblocage.

L'exécution de façon synchrone sous la forme d'un diagramme temporel:

![diagramme_temporel_q1](./img/diagramme_temporel_q1.jpg)

### **Question 2**

<u>Rappel</u>: **Asynchrone** veut dire qu'on ne sait pas quand les instructions vont se passer, cependant, on sait que cela va fini sans connaître la limite.

Il existe également un risque d'interblocage en Asynchrone.

Un diagramme temporel d'exécution possible:

![diagramme_temporel_q2](./img/diagramme_temporel_q2.jpg)

### **Question 3**

Automate / diagramme d'états de p (représentant les exécutions de p):

![automate_q3](./img/automate_q3.png)

Pour celui de q, c'est similaire, il suffit juste de remplacer A par B, et inversement.

### **Question 4**

Dessin d'un séquence d'états globaux du système qui aboutissent à un blocage:

![Dessin_q4_blocage](./img/dessin_q4_blocage.jpg)

### **Question 5**

Pour qu'il n'y ait plus de blocage, nous pouvons ôter la condition `SI` de la ligne 5.

Pour s'en convaincre, on peut faire l'algorithme et son automate.
