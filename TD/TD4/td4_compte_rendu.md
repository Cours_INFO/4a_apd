# Algorithmique Parallèle Distribuée

## **TD4 - Election dans un anneau: Algo de Peterson**

### **Question 1**

L'algorithme de Peterson est un algorithme d'élection.

<u>**Sûreté**</u>: Il y a **au plus** un processus élu.

<u>**Vivacité**</u>: Un processus est élu **en temps fini**.


![execution de l'algorithme](./img/exec_algo.jpg)

### **Question 2**

1. Preuve par contradiction:

!["par l'absurde"](./img/2-1.png)

Avec cet exemple, on voit que ce c'est pas vrai dans tous les cas. 

<i>Comment prouver qu'au moins un des neouds devient passif ?</i>

Preuve par l'**absurde**, 
 * Soit U suit des tags des noeuds au début
     - U(n) actif après => U(n-1) < U(n) et U(n-1) < U(n-2)
     - U(n-1) actif après => U(n) < U(n+1) et U(n) < U(n-1)

 U(n-1) < U(n) < U(n+1) < U(n-1) **=> Contradiction**

2. Il ne peut pas y avoir deux processus actifs protant le même tag car:
* l'id est unique.
* Par induction,
    
    - Phase 1: Initialisatioon est vraie.
    - On suppose qu'à la phase N, les actifs ont tous des ids uniques.
    - Montrons que cela est vraie pour N+1.
    - A la fin de la phase N+1, les tags reçus par les noeuds, qui sont restés actifs, sont les tags de précédents actifs qui remplissent la condition de l'algo (ligne 26). Par hypothèse, ces tags sont **inégaux**.

3. A la phase 1, tous les tags sont **inégaux** (uniques) par l'initialisation. Il y a forcément une **valeur minimale**.

A chaque phase, le noeud actif suivant le noeud ayant le tag minimal, restera forcément **actif**.

Pour conclure, la sûreté est justifié grâce à la question 2.1, et la vivacité est justifié par le fait qu'on rend toujours passif des processus à chaque tour (passifit **au moins** la moitié par phase).

### **Question 3**

Le message est stocké en **O(log(n))**, et vu que nous avons **n** processus dans l'anneau, donc nous avons **O(nlog(n))**.

### **Question 4**

L'algorithme est **correct** et cet algorithme a une complexité **plus interessante** que ceux vu en cours.
