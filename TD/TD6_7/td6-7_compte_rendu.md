# Algorithmique Parallèle Distribuée

## **TD6 & 7 - Variante de l'algo d'exclusion mutuelle de Lamport**

### **Question 1**

voir l'implementation de l'algorithme Ricart et Agrawala dans le projet [mutexRicartAgrawala](../TD2_sinalgo/sinalgo/src/projects/mutexRicartAgrawala/) dans Sinalgo.

### **Question 2**

![Diagramme Temporel](./img/diagramme_tempo.jpg)

### **Question 3**

La section critique est toujours utilisée en Exclusion Mutuelle?

* Par l'**absurde**, on suppose qu'il y a 2 noeuds en section critique.

* Notons les p et q, cela voudrait dire qu'ils ont leur valeur `Demande = true`.

* Si p est en section critique, alors p a reçu un **OK** de q, donc ddp << ddq.

* Si q est en section critique, alors q a reçu un **OK** de p, donc ddq << ddp.

=> **CONTRADICTION**

### **Question 4**

Toute demande est servie en temps fini car on ne peut pas **doubler** une demande. A chaque demande, donc à chaque tour, l'horloge du processus **augmente de 1**, ce qui évite **la famine** et permet d'**accéder** à la section critique **en temps fini**. De plus, le temps en section critique est **fini**. 

### **Question 5**

La sûreté est vérifié grâce à la question 3 et la vivacité grâce à la question 4. Donc, l'algorithme est correct.

### **Question 6**

Le nombre maximum de processus, pouvant être servis avant qu'un processus donné ne le soit à son tour, est **n**, alors n demande, donc n-1 accès à la section critique.

### **Question 7**

Il y a **2n-1 messages** par demande servie.

### **Question 8**

Cette solution est **plus efficace** que l'algorithme d'exclusion mutuelle de Lamport.

