# Algorithmique Parallèle Distribuée

## **TD8 & 9 - Circulation de jeton dans un réseau quelconque**

### **Question 1**

voir l'implementation de l'algorithme de circulation de jeton dans le projet [**token**](../TD2_sinalgo/sinalgo/src/projects/token/) dans **Sinalgo**.

- Valider que le jeton reste toujours unique.

- Tracer le parcours du jeton dans le graphe.

- Compter le nombre de messages échangés.


### **Question 2**

<u>**Sûreté**</u>: 

 * **Au plus** une décision est prise.

 * Si une décision est prise, tous les noeuds ont été **visité**.

 * **Au plus** un jeton dans le système / réseau.

<u>**Vivacité**</u>: 

 * L'algorithme se termine **en temps fini**

 * L'initiateur finit par prends une décision.

### **Question 3**

1. Il existe au plus un jeton dans le réseau.

<p>
Un seul jeton

 - Un seul initiateur par definition
 - L'initiateur envoie un jeton à un de ces voisins
 - Quand un noeud reçoit le jeton, soit il réémet sans en créer d'autres, soit il décide (pas de création de jeton).
</p>

2. Il y a au plus une décision.

<p>
De (1), nous savons que le jeton est unique. La décision ne peut être prise que par un noeud qui detient le jeton. 

Lorsque la décision est prise, le jeton ne circule plus. 
</p>

3. Chaque canal est traversé un nombre fini de fois dans chaque sens. (Préciser combien de fois au plus.)

<p>
Le jeton n'est renvoyé que sur des liens non visités. 

Si un noeud envoie sur un lien: Visited[l] = false, il le passe à true et n'y touche plus.

=> Sur le lien l, le jeton est envoyé au plus une fois dans un sens.

=> Un lien est traversé au plus 2 fois (un par sens).
</p>

4. L’exécution termine.

<p>
Nous venons de prouver dans (3) que chaque lien est traversé au plus 2 fois le nombre de liens. Ce dernier étant fini et la traversée des liens étant limité dans le temps, l'exécution se termine au bout d'un temps fini.
</p>

5. L’initiateur finit par décider.

<p>
Un non-initiateur commence par recevoir le jeton.

La première fois que ça arrive, il initie le père, puis envoie à tous les voisins (non visité), puis à la fin, il envoie au père.

Un non-initiateur ne peut pas recevoir le jeton et le garder:

Il est obligé de le renvoyer:

- Un non-initiateur ne peut commencer que par recevoir le jeton, puisque le jeton est émis dans le réseau par l'initiateur.
- Quand le non-initiateur reçoit le jeton. 
=> le nombre de receptions est égal au nombre d'envoies + 1.
</p>

6. Si une décision est prise, alors tous les processus ont été visités par le jeton.

<p>
Par l'absurde, le noeud a décidé et un lien n'a pas été visité. Mais le noeud qui a décidé a reçu lui-même le jeton par son parent
</p>


<u>**Conclusion**</u>: 

L'algorithme repond **aux spécifications**, alors l'algorithme est **correct**, et est belle et bien un **algorithme de diffusion**.

### **Question 4**

Soit m le nombre de liens dans le réseau, le nombre de messages envoyés par
l’algorithme et sa complexité en temps sont de **2m**.
