# Algorithmique Parallèle Distribuée

### **Binome:**
*  **[BONFILS Antoine](https://gitlab.com/AntoineBF)** (INFO4)
*  **[FERRARI Julien](https://gitlab.com/ferrarju)** (INFO4)

## **TD5 / Devoir maison - Sinalgo**

### **SUJET** 

1. Nous avons implémenter l'agorithme décrit dans le fichier [description.txt](../TD2_sinalgo/sinalgo/src/projects/devoir/description.txt); dans le package [Devoir](../TD2_sinalgo/sinalgo/src/projects/devoir/).

2. Cet algorithme met le booléen résultat du noeud ayant **le plus grand id** a `TRUE`, et les autres noeuds a `FALSE`. 

    <u>**Sûreté**</u>: Il y a **au plus** un processus ayant le booléen résultat a `TRUE`.

    <u>**Vivacité**</u>: Un processus aura son booléen résultat a `TRUE` **en temps fini**.

3. Tests permettant de valider la sûreté: Nous générons un anneau de 10 processus avec des **id aléatoire** et **unique**.

    (voir la methode test_random_ID() dans le fichier [CustomGlobal.java](../TD2_sinalgo/sinalgo/src/projects/devoir/CustomGlobal.java))

4. 
- (a) 

    La variable l représente **la distance** qu'un message de demande va effectuer et la variable n représente **la reception** d'un message par son propre noeud d'origine, c'est-à-dire le nombre de victoire (1 par coté). l est de la forme 2^i car il est initialisé à **1** au début, et est modifié dans **un seul cas**, à la ligne 32, où **la valeur l est doublé**. Donc la valeur de l double à chaque phase.

- (b) 

    L'identité d'un tel processus est **plus grand** que les processus qui l'entourent, car à la fin de la phase, ce processus reçoit les 2 messages <REPLY, id> de ses voisins. Donc, ces (2^i)-1 voisins à gauche et à droite (qui sont peut-être, pour certains, les mêmes si la moitié du nombre de noeuds de l'anneau est inférieur à (2^i)-1), ont des identifiants **inférieur** à notre processus. Nous pouvons justifier cela avec la condition à la **ligne 16**.

- (c) 

    L'algorithme se termine avec une valeur pour i égale à **la valeur de l'exposant de la plus petit puissance de 2 qui est supérieur au nombre de processus** dans l'anneau.

- (d) 

    Pour la **sureté**, vu que tous les noeuds ont un **id unique** et que les id sont des **entiers**, alors ils sont tous comparables entre eux (**ordre total**), donc il y aura toujours **un seul** qui sera le plus grand. 

    Pour la **vivacité**, on sait qu'au début, tous les processus envoient 2 messages, et pour qu'ils puissent renvoyer 2 autres messages à la prochaine phase, ils doivent avoir un id **plus grande** que ceux des processsus parcourus par les messsages précédemment cités. Si ce n'est pas le cas, alors ils ne ferrons que transmettre les messages des autres processus.

    De plus, l'**unicité des id** permet de savoir qu'à chaque phase, le nombre de processus qui ne font plus que transmettre **augmente**, et donc celui des processus qui sont toujours candidat, pour avec le resultat à `TRUE`, **diminue**.

5. L'algorithme a une complexité en **O(nlog(n))** messages car à la phase 0, tous les processus envoient 2 messages, si au moins un des deux messages ne revient pas, alors le processus **n'enverra plus** de messages avec son id. Donc, le processus en question ne **transmettra** que les messages des autres. Les messages de demandes iront 2 fois plus loin a chaque phase d'où la présence du log(n) car on ne se compare pas toujours avec tous les processus. On aura donc bien log(n) phases.

6. L'algorithme a une complexité en temps de l'ordre de **O(n)** par phase à cause de l'execution en **parallèle**.

7. Cet algorithme est interessant, de par **sa complexité** qui est plus basse que ceux vu en cours, voire équivalente par rapport à la complexité en messages de l'algorithme de Peterson, et de par **sa stratégie** avec les messages de demandes, qui permet de trier, de façon pertinante, les processus candidats, et les messages de reponse, qui permet de valider le retour d'un message vers le processus émetteur.